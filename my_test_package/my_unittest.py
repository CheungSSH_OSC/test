#!/usr/bin/env python
#coding:utf8

import unittest
from nihao import T

class MyTestCase(unittest.TestCase):
	def setUp(self):
		self.tmp=T() #把类实例化
	def tearDown(self):
		self.tmp=None # 从内存中清除程序
	def test_add(self):
		# 测试add方法
		self.assertEqual(100,self.tmp.add(50,50)) # 判断是否符合预期的100，如果不是则报错
if __name__=='__main__':
	unittest.main()
